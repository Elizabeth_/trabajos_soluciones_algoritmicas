#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int mostrar_menu(){
	int eleccion;
	printf("\033[1;36m");
	printf("\n\n\n\t---------------------MENU-----------------------");
	printf("\n\n\n\t	-------------------------------\n");
	printf("	\t..:::::::: 4 en linea :::::::..\n");
	printf("	\t-------------------------------\n");
	printf("\n");
	printf("		\t------------------\n");
	printf("	\t	Modalidad de juego\n");
	printf("		\t------------------\n");
	printf("		\t<1> Vs computador\n");
	printf("		\t<2> 1 VS 1\n");
	printf("		\t<3> SALIR\n");
	printf("\n");
	printf("\t------------------------------------------------");
	printf("\n");
	printf("\tIngrese opcion: ");
	printf("\033[0m");
	scanf("		%d",&eleccion);

	return eleccion;		
}

void llenar_tablero( int n, int tablero[n][n]){
	
	for(int i=0; i<n;i++){
		for (int j=0; j<n; j++){
			tablero[i][j] = 32;
		}
	}
}

 void imprimir_tablero(int n, int tablero[n][n]){
	 int i=0, j=0;
	printf("\n\n\n              \t***JUEGO 4 EN LINEA***\n");
	printf("\n");
	for(i=0; i<n;i++){
		for (j=0; j<n; j++){
			if(tablero[i][j] == 79){
				printf("\033[1;32m");
				printf("\t[  %c  ]",tablero[i][j]);
				printf("\033[0m");
			}
			else if(tablero[i][j] == 88){
				printf("\033[1;33m");
				printf("\t[  %c  ]",tablero[i][j]);
				printf("\033[0m");
			}
			else{
				printf("\033[1;34m");
				printf("\t[  %c  ]",tablero[i][j]);
				printf("\033[0m");
			}
		}

		if(i!=7){
			printf ("\n");
			printf("\033[1;33m");
			printf("\t-------------------------------------------------------------");
			printf("\033[0m");
			printf("\n");
		}
		else{
			printf("\033[0;34m");
			printf("\n\t-------------------------------------------------------------------");
			printf("\033[0m");		
			printf("\033[0;35m");
			printf("\n\t[  1  ] [  2  ] [  3  ] [  4  ] [  5  ] [  6  ] [  7  ] [  8  ]\n");
			printf("\033[0m");		
			
		}
	}
}
int pedir_columna_seleccionada(){
	int columna_seleccionada; 
	printf("Ingrese columna_seleccionada: ");
	scanf("%d", &columna_seleccionada);
	columna_seleccionada--;
	return columna_seleccionada;
}

int validarFila(int n , int tablero[n][n], int columna_usuario){
	int filaValida, i;
	for (i=7; i >= 0; i--){
		if (tablero[i][columna_usuario] == 32){
			filaValida = i;
			break;
		}
		if (tablero[i][columna_usuario] != 32){
			filaValida= -1;			
					
		}
	}
	return filaValida;
	
}
int verificar_vertical(int filaValida, int columna_usuario, int tipo_ficha, int n, int tablero[n][n], int c, char nombre_jugador[c]){
	int encontrado=0;
	if (tablero[filaValida][columna_usuario]== tipo_ficha && tablero[filaValida+1][columna_usuario]==tipo_ficha && tablero[filaValida+2][columna_usuario]==tipo_ficha && tablero[filaValida +3][columna_usuario]== tipo_ficha){ 
		printf("\033[0;36m");
		printf("\n\n \t-------------- GANO EL JUGADOR %s --------------\n", nombre_jugador);
		printf("\033[0m");	
		encontrado=1;
	}
	return encontrado;
}

int verificar_horizontal(int filaValida, int columna_usuario, int tipo_ficha, int n, int tablero[n][n], int c, char nombre_jugador[c]){
	int encontrado=0;
	int i;
	for (i=0; i<columna_usuario+1;i++) {
		if (tablero[filaValida][i]==tipo_ficha  && tablero[filaValida][i+1]==tipo_ficha && tablero[filaValida][i+2]==tipo_ficha && tablero[filaValida][i+3]==tipo_ficha){
			printf("\033[0;36m");
			printf("\n\n -------------- GANO EL JUGADOR %s --------------\n", nombre_jugador);
			printf("\033[0m");
			encontrado=1;
		}
	}
	return encontrado;
	//horizontal-
}

int verificar_diagonal1(int filaValida, int columna_usuario, int tipo_ficha, int n, int tablero[n][n], int c, char nombre_jugador[c]){
	//diagonal desde izquierda superior a derecha inferior.
	int encontrado=0;
	int i;
	int j;
	for (i=3; i<n;i++) {
		for (j=3; j<n; j++) {
			if (tablero[i][j]== tipo_ficha && tablero[i-1][j-1]== tipo_ficha && tablero[i-2][j-2]== tipo_ficha && tablero[i-3][j-3]==tipo_ficha){ 
				printf("\033[0;36m");
				printf("\n\n -------------- GANO EL JUGADOR %s --------------\n", nombre_jugador);
				printf("\033[0m");
				encontrado=1;
			}
		}
	}
	// diagonal desde inferior derecha a superior izquierda.
	
	for (i=3; i<n;i++) {
		for (j=0; j<5; j++) {
			if (tablero[i][j]== tipo_ficha && tablero[i -1][j+1]== tipo_ficha && tablero[i-2][j+2]== tipo_ficha && tablero[i-3][j+3]==tipo_ficha){ 
				printf("\033[0;36m");
				printf("\n\n -------------- GANO EL JUGADOR %s --------------\n", nombre_jugador);
				printf("\033[0m");
				encontrado=1;
			}
		}
	}
	
	return encontrado;
}

int Uno_vs_uno(int n, int tablero[n][n]){ 
	int winner=0;
	int columna_usuario; //columna seleccionada por el jugador
	int contador_turnos=0;
	int encontrado=0;
	int filaValida;
	int tipo_ficha=100;
	int c=20;
	char nombre_jugador1[c];
	char nombre_jugador2[c]; 
	printf("Ingrese nombre del jugador 1: " );
	scanf("%s", nombre_jugador1);
	printf("Ingrese nombre del jugador 2: " );
	scanf("%s", nombre_jugador2);
	printf("\n\n\n");
	llenar_tablero(n,tablero);
	imprimir_tablero(n,tablero);

	int turno;
	turno = rand() % 2; //entrega numeros entre 0 y 1
	
	while(encontrado==0 && contador_turnos!=(n*n)){ //encuentra el jugador ganador
		if (turno==0){ //turno del jugador 1
			printf(" Juega:  %s\n" ,nombre_jugador1);
			columna_usuario = pedir_columna_seleccionada();
			filaValida = validarFila(n, tablero, columna_usuario);
			
			if (filaValida==-1){ //valida que la columna no este llena	
				while (filaValida==-1){ //pide que se ingrese una columna hasta que sea válida
					printf("No puede ingresar mas fichas en esta columna\n");
					columna_usuario = pedir_columna_seleccionada();	
					filaValida = validarFila(n, tablero, columna_usuario);
				}	
				
			}
			else { //si se puede ingresar una ficha en la columna entonces
				tablero[filaValida][columna_usuario]= 88;
				imprimir_tablero(n, tablero);	
			}
			tipo_ficha=88;
			//verificar ganador
			if (encontrado==0) {
				encontrado=verificar_vertical(filaValida, columna_usuario, tipo_ficha, n, tablero, c, nombre_jugador1);
			} 
			if (encontrado==0) {
				encontrado=verificar_horizontal (filaValida, columna_usuario, tipo_ficha, n, tablero, c, nombre_jugador1);
			}
			if (encontrado==0) {
				encontrado=verificar_diagonal1 (filaValida, columna_usuario, tipo_ficha, n, tablero, c, nombre_jugador1);
			}
			//aun no gana entonces le tocara al otro jugador
			if (encontrado==1) { //si el jugador 1 gano entonces se para el ciclo
				winner=1;
				break;
			} else { // sino le toca al jugador 2
				turno = 1;
				contador_turnos++;	
			}
		} else if (turno==1){ //turno del jugador 2
	
			printf(" Juega: %s \n" ,nombre_jugador2);
			columna_usuario = pedir_columna_seleccionada();
			filaValida = validarFila(n, tablero, columna_usuario);
			
			if (filaValida==-1){
				printf("No puede ingresar mas fichas en esta columna\n");	
				while (filaValida==-1){
					columna_usuario = pedir_columna_seleccionada();	
					filaValida = validarFila(n, tablero, columna_usuario);
				}
			}
			else { //si se puede ingresar una ficha en la columna entonces
				tablero[filaValida][columna_usuario]= 79;
				imprimir_tablero(n, tablero);	
			}
			tipo_ficha=79;
			//verifica si el jugador 2 ha ganado
			if (encontrado==0) {
				encontrado=verificar_vertical(filaValida, columna_usuario, tipo_ficha, n, tablero, c, nombre_jugador2);
			} 
			if (encontrado==0) {
				encontrado=verificar_horizontal (filaValida, columna_usuario, tipo_ficha, n, tablero, c, nombre_jugador2);
			}
			if (encontrado==0) {
				encontrado=verificar_diagonal1 (filaValida, columna_usuario, tipo_ficha, n, tablero, c, nombre_jugador2);
			}
			if (encontrado==1) { //si el jugador 1 gano entonces se para el ciclo
				winner=1;
				break;
			} else { // sino le toca al jugador 2
				turno = 0;
				contador_turnos++;
			}
			if (contador_turnos==(n*n)) {
				printf ("\n\n ----------- EMPATE -----------\n");
			} 
		}
		
		
	}
	return winner; // 
}

int Uno_vs_pc(int n, int tablero[n][n]){ 
	int winner=0;
	int columna_usuario; //columna seleccionada por el jugador
	int contador_turnos=0;
	int encontrado=0;
	int filaValida;
	int tipo_ficha=100;
	int c=20;
	char nombre_jugador1[c];
	char nombre_jugador2[c];
	printf("Ingrese nombre del jugador 1: " );
	scanf("%s", nombre_jugador1);
	printf("Nombre del jugador 2: pc_eli" );
	nombre_jugador2[0]='p'; // para colocar cada letra en cada posicion del arreglo del nombre. 
	nombre_jugador2[1]='c';
	nombre_jugador2[2]='_';
	nombre_jugador2[3]='e';
	nombre_jugador2[4]='l';
	nombre_jugador2[5]='i';
	printf("\n\n\n");
	llenar_tablero(n,tablero);
	imprimir_tablero(n,tablero);

	int turno;
	turno = rand() % 2; //entrega numeros entre 0 y 1
	
	while(encontrado==0 && contador_turnos!=(n*n)){ //encuentra el jugador ganador
		if (turno==0){ //turno del jugador 1
			printf(" Juega:  %s\n" ,nombre_jugador1);
			columna_usuario = pedir_columna_seleccionada();
			filaValida = validarFila(n, tablero, columna_usuario);
			
			if (filaValida==-1){ //valida que la columna no este llena	
				while (filaValida==-1){ //pide que se ingrese una columna hasta que sea válida
					printf("No puede ingresar mas fichas en esta columna\n");
					columna_usuario = pedir_columna_seleccionada();	
					filaValida = validarFila(n, tablero, columna_usuario);
				}	
				
			}
			else { //si se puede ingresar una ficha en la columna entonces
				tablero[filaValida][columna_usuario]= 88;
				imprimir_tablero(n, tablero);	
			}
			tipo_ficha=88;
			//verificar ganador
			if (encontrado==0) {
				encontrado=verificar_vertical(filaValida, columna_usuario, tipo_ficha, n, tablero, c, nombre_jugador1);
			} 
			if (encontrado==0) {
				encontrado=verificar_horizontal (filaValida, columna_usuario, tipo_ficha, n, tablero, c, nombre_jugador1);
			}
			if (encontrado==0) {
				encontrado=verificar_diagonal1 (filaValida, columna_usuario, tipo_ficha, n, tablero, c, nombre_jugador1);
			}
			//aun no gana entonces le tocara al otro jugador
			if (encontrado==1) { //si el jugador 1 gano entonces se para el ciclo
				winner=1;
				break;
			} else { // sino le toca al jugador 2
				turno = 1;
				contador_turnos++;	
			}
		} else if (turno==1){ //turno del PC
	
			printf(" Juega: %s \n",nombre_jugador2);
			columna_usuario = rand()%n; //el computador elije una columna entre 0-N
			columna_usuario=columna_usuario+1;
			filaValida = validarFila(n, tablero, columna_usuario);
			
			if (filaValida==-1){
				printf("No puede ingresar mas fichas en esta columna\n");	
				while (filaValida==-1){
					columna_usuario = pedir_columna_seleccionada();	
					filaValida = validarFila(n, tablero, columna_usuario);
				}
			}
			else { //si se puede ingresar una ficha en la columna entonces
				tablero[filaValida][columna_usuario]= 79;
				imprimir_tablero(n, tablero);	
			}
			tipo_ficha=79;
			//verifica si el jugador 2 ha ganado
			if (encontrado==0) {
				encontrado=verificar_vertical(filaValida, columna_usuario, tipo_ficha, n, tablero, c, nombre_jugador2);
			} 
			if (encontrado==0) {
				encontrado=verificar_horizontal (filaValida, columna_usuario, tipo_ficha, n, tablero, c, nombre_jugador2);
			}
			if (encontrado==0) {
				encontrado=verificar_diagonal1 (filaValida, columna_usuario, tipo_ficha, n, tablero, c, nombre_jugador2);
			}
			if (encontrado==1) { //si alguien gano entonces se para el ciclo
				winner=1;
				break;
			} else { // sino le toca al jugador 1
				turno = 0;
				contador_turnos++;
			}
			if (contador_turnos==(n*n)) {
				printf ("\n\n ----------- EMPATE -----------\n");
			} 
		}
		
		
	}
	return winner;
}


int main (){
	int eleccion;
	int n=8; //limite de la matriz
	int tablero[n][n];
	int winner=0;
	eleccion=mostrar_menu();
	while (winner==0 && eleccion!=3) {
		if (eleccion==1){
			Uno_vs_pc(n, tablero);	
		} else if (eleccion==2){
				winner=Uno_vs_uno(n, tablero);	
		} else if (eleccion==3){
				printf("\t***	Gracias por Jugar	***\n	***	Hasta pronto :)		***");
		}
		eleccion=mostrar_menu();
		winner=0;
	}
	return 0; 
}
